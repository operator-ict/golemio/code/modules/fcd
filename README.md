<div align="center">
<p>
    <a href="https://operatorict.cz">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/core/-/raw/development/.assets/oict_logo.png" alt="oict" width="100px" height="100px" />
    </a>
    <a href="https://golemio.cz">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/core/-/raw/development/.assets/golemio_logo.png" alt="golemio" width="100px" height="100px" />
    </a>
</p>

<h1>@golemio/fcd</h1>

<p>
    <a href="https://gitlab.com/operator-ict/golemio/code/modules/fcd/commits/master">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/fcd/badges/master/pipeline.svg" alt="pipeline">
    </a>
    <a href="https://gitlab.com/operator-ict/golemio/code/modules/fcd/commits/master">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/fcd/badges/master/coverage.svg" alt="coverage">
    </a>
    <a href="./LICENSE">
        <img src="https://img.shields.io/npm/l/@golemio/fcd" alt="license">
    </a>
</p>

<p>
    <a href="#installation">Installation</a> · <a href="./docs">Documentation</a> · <a href="https://operator-ict.gitlab.io/golemio/code/modules/fcd">TypeDoc</a>
</p>
</div>

This module is intended for use with Golemio services. Refer [here](https://gitlab.com/operator-ict/golemio/code/modules/core/-/blob/development/README.md) for further information on usage, local development and more.

## Installation

The APIs may be unstable. Therefore, we recommend to install this module as an exact version.

```bash
# Latest version
npm install --save-exact @golemio/fcd@latest

# Development version
npm install --save-exact @golemio/fcd@dev
```

<!-- ## Description -->

<!-- Insert module-specific info here -->
