// Load reflection lib
require("@golemio/core/dist/shared/_global");

const path = require("path");
require("ts-node").register({ project: path.resolve(process.cwd(), "tsconfig.json") });
require("tsconfig-paths").register();

const fs = require("fs");
const http = require("http");
const express = require("express");
const xml2js = require("xml2js");
const { AMQPConnector } = require("@golemio/core/dist/input-gateway/connectors");

const app = express();
const server = http.createServer(app);

const bodyParser = require("body-parser");
require("body-parser-xml")(bodyParser);

const start = async () => {
    await AMQPConnector.connect();

    app.use((req, res, next) => {
        const FloatingCarDataInput = fs.readFileSync("test/data/floating_car_data.xml", { encoding: "utf-8" });
        const parser = new xml2js.Parser({
            strict: true,
            explicitArray: false,
            normalize: false,
            normalizeTags: false,
        });
        parser.parseString(FloatingCarDataInput, function (err, result) {
            req.body = result;
            if (err) {
                log.silly("Error caught by the router error handler.");
            }
            next();
        });
    });

    const { FloatingCarDataRouter } = require("#ig/FloatingCarDataRouter");

    app.use("/fcd", new FloatingCarDataRouter().router);

    return new Promise((resolve) => {
        server.listen(3011, () => {
            resolve();
        });
    });
};

const stop = async () => {
    await AMQPConnector.disconnect();
    server.close();
};

module.exports = {
    start,
    stop,
};
