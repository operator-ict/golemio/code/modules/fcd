// Load reflection lib
import "@golemio/core/dist/shared/_global";

import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { OutputGatewayContainer, ContainerToken } from "@golemio/core/dist/output-gateway/ioc";

OutputGatewayContainer.resolve<IDatabaseConnector>(ContainerToken.PostgresDatabase).connect();
