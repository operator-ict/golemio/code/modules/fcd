import sinon from "sinon";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { OutputFloatingCarDataTransformation } from "#og/transformations/OutputFloatingCarDataTransformation";
import { IFloatingCarWithOsmPathDataModel } from "#sch/interfaces/FloatingCarDataInterface";

chai.use(chaiAsPromised);

describe("OutputFloatingCarDataTransformation", () => {
    let sandbox: any = null;

    const sourceData: IFloatingCarWithOsmPathDataModel = {
        publication_time: "2022-09-24T10:50:38.000Z",
        source_identification: "TS10125T10123",
        measurement_or_calculation_time: "2022-09-24T10:50:38.000Z",
        predefined_location: "TS10125T10123",
        version_of_predefined_location: 2,
        traffic_level: "level2",
        data_quality: 0.5,
        input_values: 1,
        average_vehicle_speed: 38,
        travel_time: 22,
        free_flow_travel_time: 19,
        free_flow_speed: 47,
        osm_path: "[10125, 10123]",
    };

    const transformation = new OutputFloatingCarDataTransformation();

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    it("transforms data correctly with osmPath", () => {
        const actual = transformation.transform([sourceData], true);
        const expected = {
            modelBaseVersion: "3",
            payloadPublicationLight: {
                lang: "cz",
                publicationTime: "2022-09-24T12:50:38+02:00",
                publicationCreator: {
                    country: "cz",
                    nationalIdentifier: "ŘSD",
                },
                elaboratedData: [
                    {
                        averageVehicleSpeed: "38",
                        freeFlowSpeed: "47",
                        freeFlowTravelTime: "19",
                        measurementOrCalculationTime: "2022-09-24T12:50:38+02:00",
                        numberOfInputValuesUsed: "1",
                        predefinedLocationId: "TS10125T10123",
                        sourceIdentification: "TS10125T10123",
                        supplierCalculatedDataQuality: "0.5",
                        trafficLevel: "level2",
                        travelTime: "22",
                        osmPath: [10125, 10123],
                    },
                ],
            },
        };
        expect(actual).to.deep.equal(expected);
    });

    it("transforms data correctly without osmPath", () => {
        const actual = transformation.transform([sourceData], false);
        const expected = {
            modelBaseVersion: "3",
            payloadPublicationLight: {
                lang: "cz",
                publicationTime: "2022-09-24T12:50:38+02:00",
                publicationCreator: {
                    country: "cz",
                    nationalIdentifier: "ŘSD",
                },
                elaboratedData: [
                    {
                        averageVehicleSpeed: "38",
                        freeFlowSpeed: "47",
                        freeFlowTravelTime: "19",
                        measurementOrCalculationTime: "2022-09-24T12:50:38+02:00",
                        numberOfInputValuesUsed: "1",
                        predefinedLocationId: "TS10125T10123",
                        sourceIdentification: "TS10125T10123",
                        supplierCalculatedDataQuality: "0.5",
                        trafficLevel: "level2",
                        travelTime: "22",
                    },
                ],
            },
        };
        expect(actual).to.deep.equal(expected);
    });
});
