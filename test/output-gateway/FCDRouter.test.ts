import {
    FCDRouter,
    FloatingCarDataModel,
    FloatingCarExtendedDataModel,
    FloatingCarWithOsmPathDataModel,
    FloatingCarWithOsmPathExtendedDataModel,
} from "#og";
import { OutputFloatingCarDataTransformation } from "#og/transformations/OutputFloatingCarDataTransformation";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import sinon from "sinon";
import request from "supertest";

chai.use(chaiAsPromised);

describe("FCDRouter", () => {
    // Create clean express instance
    const app = express();
    // Basic configuration: create a sinon sandbox for testing
    let sandbox: any = null;
    const router = new FCDRouter(
        new FloatingCarDataModel(),
        new FloatingCarWithOsmPathDataModel(),
        new FloatingCarExtendedDataModel(),
        new FloatingCarWithOsmPathExtendedDataModel(),
        new OutputFloatingCarDataTransformation()
    );

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    before(() => {
        // Mount the tested router to the express instance
        app.use("/fcd", router.router);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with json to GET /fcd/info", (done) => {
        request(app)
            .get("/fcd/info")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .set("Accept", "application/json")
            .expect("cache-control", "public, s-maxage=180, stale-while-revalidate=60")
            .expect(200, done);
    });

    it("should respond with correct json on /fcd/info", (done) => {
        const expectedResult = JSON.parse(fs.readFileSync(__dirname + "/data/fcdresult.json").toString("utf8"));
        const appResult = request(app).get("/fcd/info?limit=20&osmPath=true").set("Accept", "application/json");
        appResult.expect(expectedResult, done);
    });
});
