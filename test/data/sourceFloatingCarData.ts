export const sourceFloatingCarData = {
    d2LogicalModel: {
        $: {
            modelBaseVersion: "2",
            "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
            xmlns: "http://datex2.eu/schema/2/2_0",
        },
        exchange: {
            supplierIdentification: {
                country: "cz",
                nationalIdentifier: "ŘSD",
            },
        },
        payloadPublication: {
            $: {
                "xsi:type": "ElaboratedDataPublication",
                lang: "cs",
            },
            publicationTime: "2021-09-21T00:00:18+02:00",
            publicationCreator: {
                country: "cz",
                nationalIdentifier: "ŘSD",
            },
            headerInformation: {
                confidentiality: "noRestriction",
                informationStatus: "real",
            },
            elaboratedData: [
                {
                    source: {
                        sourceIdentification: "TS01225T25680",
                    },
                    basicData: {
                        $: {
                            "xsi:type": "TrafficStatus",
                        },
                        measurementOrCalculationTime: "2021-09-21T00:00:01+02:00",
                        pertinentLocation: {
                            $: {
                                "xsi:type": "LocationByReference",
                            },
                            predefinedLocationReference: {
                                $: {
                                    targetClass: "PredefinedLocation",
                                    id: "TS01225T25680",
                                    version: "2",
                                },
                            },
                        },
                        trafficStatusExtension: {
                            ndicFcdExtension: {
                                trafficLevel: {
                                    $: {
                                        supplierCalculatedDataQuality: "0.500",
                                        numberOfInputValuesUsed: "1",
                                    },
                                    trafficLevelValue: "level3",
                                },
                            },
                        },
                    },
                },
                {
                    source: {
                        sourceIdentification: "TS01225T25680",
                    },
                    basicData: {
                        $: {
                            "xsi:type": "TrafficSpeed",
                        },
                        measurementOrCalculationTime: "2021-09-21T00:00:01+02:00",
                        pertinentLocation: {
                            $: {
                                "xsi:type": "LocationByReference",
                            },
                            predefinedLocationReference: {
                                $: {
                                    targetClass: "PredefinedLocation",
                                    id: "TS01225T25680",
                                    version: "2",
                                },
                            },
                        },
                        averageVehicleSpeed: {
                            $: {
                                supplierCalculatedDataQuality: "0.500",
                                numberOfInputValuesUsed: "1",
                            },
                            speed: "88",
                        },
                    },
                },
                {
                    source: {
                        sourceIdentification: "TS01225T25680",
                    },
                    basicData: {
                        $: {
                            "xsi:type": "TravelTimeData",
                        },
                        measurementOrCalculationTime: "2021-09-21T00:00:01+02:00",
                        pertinentLocation: {
                            $: {
                                "xsi:type": "LocationByReference",
                            },
                            predefinedLocationReference: {
                                $: {
                                    targetClass: "PredefinedLocation",
                                    id: "TS01225T25680",
                                    version: "2",
                                },
                            },
                        },
                        travelTime: {
                            $: {
                                supplierCalculatedDataQuality: "0.500",
                                numberOfInputValuesUsed: "1",
                            },
                            duration: "133",
                        },
                        freeFlowTravelTime: {
                            $: {
                                supplierCalculatedDataQuality: "0.500",
                                numberOfInputValuesUsed: "1",
                            },
                            duration: "91",
                        },
                        freeFlowSpeed: {
                            $: {
                                supplierCalculatedDataQuality: "0.500",
                                numberOfInputValuesUsed: "1",
                            },
                            speed: "130",
                        },
                    },
                },
                {
                    source: {
                        sourceIdentification: "TS37613T37027",
                    },
                    basicData: {
                        $: {
                            "xsi:type": "TrafficStatus",
                        },
                        measurementOrCalculationTime: "2021-09-21T00:00:01+02:00",
                        pertinentLocation: {
                            $: {
                                "xsi:type": "LocationByReference",
                            },
                            predefinedLocationReference: {
                                $: {
                                    targetClass: "PredefinedLocation",
                                    id: "TS37613T37027",
                                    version: "2",
                                },
                            },
                        },
                        trafficStatusExtension: {
                            ndicFcdExtension: {
                                trafficLevel: {
                                    $: {
                                        supplierCalculatedDataQuality: "0.500",
                                        numberOfInputValuesUsed: "1",
                                    },
                                    trafficLevelValue: "level3",
                                },
                            },
                        },
                    },
                },
                {
                    source: {
                        sourceIdentification: "TS37613T37027",
                    },
                    basicData: {
                        $: {
                            "xsi:type": "TrafficSpeed",
                        },
                        measurementOrCalculationTime: "2021-09-21T00:00:01+02:00",
                        pertinentLocation: {
                            $: {
                                "xsi:type": "LocationByReference",
                            },
                            predefinedLocationReference: {
                                $: {
                                    targetClass: "PredefinedLocation",
                                    id: "TS37613T37027",
                                    version: "2",
                                },
                            },
                        },
                        averageVehicleSpeed: {
                            $: {
                                supplierCalculatedDataQuality: "0.500",
                                numberOfInputValuesUsed: "1",
                            },
                            speed: "50",
                        },
                    },
                },
                {
                    source: {
                        sourceIdentification: "TS37613T37027",
                    },
                    basicData: {
                        $: {
                            "xsi:type": "TravelTimeData",
                        },
                        measurementOrCalculationTime: "2021-09-21T00:00:01+02:00",
                        pertinentLocation: {
                            $: {
                                "xsi:type": "LocationByReference",
                            },
                            predefinedLocationReference: {
                                $: {
                                    targetClass: "PredefinedLocation",
                                    id: "TS37613T37027",
                                    version: "2",
                                },
                            },
                        },
                        travelTime: {
                            $: {
                                supplierCalculatedDataQuality: "0.500",
                                numberOfInputValuesUsed: "1",
                            },
                            duration: "46",
                        },
                        freeFlowTravelTime: {
                            $: {
                                supplierCalculatedDataQuality: "0.500",
                                numberOfInputValuesUsed: "1",
                            },
                            duration: "34",
                        },
                        freeFlowSpeed: {
                            $: {
                                supplierCalculatedDataQuality: "0.500",
                                numberOfInputValuesUsed: "1",
                            },
                            speed: "73",
                        },
                    },
                },
            ],
        },
    },
};
