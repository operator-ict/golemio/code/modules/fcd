import { SinonSandbox, createSandbox, SinonSpy } from "sinon";
import { FCDWorker } from "#ie/workers/FCDWorker";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { sourceFloatingCarData } from "../data/sourceFloatingCarData";
import { floatingCarDataTransformed } from "../data/floatingCarDataTransformed";
import { expect } from "chai";

describe("FCDWorker", () => {
    let sandbox: SinonSandbox;
    let worker: FCDWorker;

    beforeEach(() => {
        sandbox = createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().callsFake(() => ({})),
                transaction: sandbox.stub().callsFake(() => Object.assign({ commit: sandbox.stub() })),
            })
        );

        worker = new FCDWorker();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("getQueuePrefix", () => {
        it("should have correct queue prefix set", () => {
            const result = worker["getQueuePrefix"]();
            expect(result).to.contain(".fcd");
        });
    });

    describe("getQueueDefinition", () => {
        it("should return correct queue definition", () => {
            const result = worker.getQueueDefinition();
            expect(result.name).to.equal("Fcd");
            expect(result.queues.length).to.equal(3);
        });
    });

    describe("registerTask", () => {
        it("should have three tasks registered", () => {
            expect(worker["queues"].length).to.equal(3);

            expect(worker["queues"][0].name).to.equal("saveFloatingCarData");
            expect(worker["queues"][1].name).to.equal("saveRegionsData");
            expect(worker["queues"][2].name).to.equal("regionsDataRetention");

            expect(worker["queues"][0].options.messageTtl).to.equal(59 * 60 * 1000);
            expect(worker["queues"][1].options.messageTtl).to.equal(59 * 60 * 1000);
            expect(worker["queues"][2].options.messageTtl).to.equal(59 * 60 * 1000);
        });
    });
});
