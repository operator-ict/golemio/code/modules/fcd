import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { FloatingCarDataTransformation } from "#ie";
import { sourceFloatingCarData } from "../data/sourceFloatingCarData";
import { floatingCarDataTransformed } from "../data/floatingCarDataTransformed";

chai.use(chaiAsPromised);

describe("TrafficInfoTransformation", () => {
    let transformation: FloatingCarDataTransformation;

    beforeEach(async () => {
        transformation = new FloatingCarDataTransformation();
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("FloatingCarData");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        const msgParsed = sourceFloatingCarData.d2LogicalModel;
        const data = await transformation.transform(msgParsed);
        expect(JSON.parse(JSON.stringify(data))).to.eql(floatingCarDataTransformed); // removes all undefined for chai deep check
    });
});
