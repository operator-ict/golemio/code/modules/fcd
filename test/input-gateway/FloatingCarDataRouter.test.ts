import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import express, { Request, Response, NextFunction } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler } from "@golemio/core/dist/shared/golemio-errors";
import { AMQPConnector } from "@golemio/core/dist/input-gateway/connectors";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { floatingCarDataRouter } from "#ig/FloatingCarDataRouter";
import * as fs from "fs";
import sinon, { SinonSandbox } from "sinon";

const bodyParser = require("body-parser");
require("body-parser-xml")(bodyParser);

chai.use(chaiAsPromised);

describe("Floating Car Data Router Tests", () => {
    const app = express();
    let sandbox: SinonSandbox;
    app.use(
        bodyParser.xml({
            limit: "2MB",
            xmlParseOptions: {
                strict: true,
                explicitArray: false,
                normalize: true,
                normalizeTags: false, // Transform tags to lowercase
            },
        })
    );

    const FloatingCarDataInput = fs.readFileSync(__dirname + "/../data/floating_car_data.xml", { encoding: "utf-8" });

    before(async () => {
        sandbox = sinon.createSandbox();
        sandbox.stub(AMQPConnector, "connect");
        await AMQPConnector.connect();

        app.use("/fcd", floatingCarDataRouter);
        app.use((err: any, _req: Request, res: Response, _next: NextFunction) => {
            const error = HTTPErrorHandler.handle(err, log);
            if (error) {
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(error.error_status || 500).send(error);
            }
        });
    });

    describe("Floating Car Data Router Tests", () => {
        it("should respond with 204 to POST /fcd/fcd-info (text/xml)", (done) => {
            request(app)
                .post("/fcd/fcd-info")
                .send(FloatingCarDataInput)
                .set("Content-Type", "text/xml; charset=utf-8")
                .expect(204, done());
        });

        it("should respond with 204 to POST /fcd/fcd-info (application/xml)", (done) => {
            request(app)
                .post("/fcd/fcd-info")
                .send(FloatingCarDataInput)
                .set("Content-Type", "application/xml; charset=utf-8")
                .expect(204, done());
        });

        it("should respond with 406 to POST /fcd/fcd-info with bad content type", (done) => {
            request(app)
                .post("/fcd/fcd-info")
                .send("value=0") // x-www-form-urlencoded upload
                .expect(406, done());
        });

        it("should respond with 406 to POST /fcd/fcd-info with no content type", (done) => {
            request(app).post("/fcd/fcd-info").send(FloatingCarDataInput).unset("Content-Type").expect(406, done());
        });

        it("should respond with 422 to POST /fcd/fcd-info with invalid data", (done) => {
            request(app)
                .post("/fcd/fcd-info")
                .send("<invalid-data></invalid-data>")
                .set("Content-Type", "text/xml; charset=utf-8")
                .expect(422, done());
        });
    });
});
