import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { FcdPragueController } from "#ig/FcdPragueController";
import { sourceFloatingCarData } from "../data/sourceFloatingCarData";
import { config } from "@golemio/core/dist/input-gateway";
import { FCD } from "#sch";

chai.use(chaiAsPromised);

describe("Floating CarData Controller Tests", () => {
    let sandbox: SinonSandbox;
    let controller: FcdPragueController;
    let queuePrefix: string;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        queuePrefix = config.rabbit_exchange_name + "." + FCD.name.toLowerCase();
        controller = new FcdPragueController();
        sandbox.stub(controller["validator"], "Validate").callsFake(() => Promise.resolve(true));
        sandbox.stub(controller, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should have name", () => {
        expect(controller.name).not.to.be.undefined;
    });

    it("should have processData method", () => {
        expect(controller.processData).not.to.be.undefined;
    });

    it("should properly process data", async () => {
        await controller.processData(sourceFloatingCarData);
        sandbox.assert.calledOnce(controller["validator"].Validate as SinonSpy);
        sandbox.assert.calledOnce(controller["sendMessageToExchange"] as SinonSpy);
        sandbox.assert.calledWith(
            controller["sendMessageToExchange"] as SinonSpy,
            "input." + queuePrefix + ".saveFloatingCarData",
            JSON.stringify(sourceFloatingCarData.d2LogicalModel),
            { persistent: true }
        );
    });
});
