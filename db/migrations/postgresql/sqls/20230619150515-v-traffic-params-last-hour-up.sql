-- fcd.v_traffic_params_last_hour view source

CREATE OR REPLACE VIEW fcd.v_traffic_params_last_hour AS (
    SELECT *
    FROM  (
        SELECT
            DISTINCT ON (tr_all.source_identification)
            tr_all.publication_time,
            tr_all.source_identification,
            tr_all.measurement_or_calculation_time,
            tr_all.predefined_location,
            tr_all.version_of_predefined_location,
            tr_all.traffic_level,
            tr_all.queue_exists,
            tr_all.queue_length,
            tr_all.from_point,
            tr_all.to_point,
            tr_all.data_quality,
            tr_all.input_values,
            tr_all.average_vehicle_speed,
            tr_all.travel_time,
            tr_all.free_flow_travel_time,
            tr_all.free_flow_speed,
            tr_all.create_batch_id,
            tr_all.created_at,
            tr_all.created_by,
            tr_all.update_batch_id,
            tr_all.updated_at,
            tr_all.updated_by
        FROM (
            SELECT
                fcd_traff_params_part.publication_time,
                fcd_traff_params_part.source_identification,
                fcd_traff_params_part.measurement_or_calculation_time,
                fcd_traff_params_part.predefined_location,
                fcd_traff_params_part.version_of_predefined_location,
                fcd_traff_params_part.traffic_level,
                fcd_traff_params_part.queue_exists,
                fcd_traff_params_part.queue_length,
                fcd_traff_params_part.from_point,
                fcd_traff_params_part.to_point,
                fcd_traff_params_part.data_quality,
                fcd_traff_params_part.input_values,
                fcd_traff_params_part.average_vehicle_speed,
                fcd_traff_params_part.travel_time,
                fcd_traff_params_part.free_flow_travel_time,
                fcd_traff_params_part.free_flow_speed,
                fcd_traff_params_part.create_batch_id,
                fcd_traff_params_part.created_at,
                fcd_traff_params_part.created_by,
                fcd_traff_params_part.update_batch_id,
                fcd_traff_params_part.updated_at,
                fcd_traff_params_part.updated_by
            FROM fcd.fcd_traff_params_part
            WHERE fcd_traff_params_part.measurement_or_calculation_time >= NOW() - INTERVAL '1 HOUR'
            UNION ALL
            SELECT
                fcd_traff_params_regions.publication_time,
                fcd_traff_params_regions.source_identification,
                fcd_traff_params_regions.measurement_or_calculation_time,
                fcd_traff_params_regions.predefined_location,
                fcd_traff_params_regions.version_of_predefined_location,
                fcd_traff_params_regions.traffic_level,
                fcd_traff_params_regions.queue_exists,
                fcd_traff_params_regions.queue_length,
                fcd_traff_params_regions.from_point,
                fcd_traff_params_regions.to_point,
                fcd_traff_params_regions.data_quality,
                fcd_traff_params_regions.input_values,
                fcd_traff_params_regions.average_vehicle_speed,
                fcd_traff_params_regions.travel_time,
                fcd_traff_params_regions.free_flow_travel_time,
                fcd_traff_params_regions.free_flow_speed,
                fcd_traff_params_regions.create_batch_id,
                fcd_traff_params_regions.created_at,
                fcd_traff_params_regions.created_by,
                fcd_traff_params_regions.update_batch_id,
                fcd_traff_params_regions.updated_at,
                fcd_traff_params_regions.updated_by
            FROM fcd.fcd_traff_params_regions
            WHERE fcd_traff_params_regions.measurement_or_calculation_time >= NOW() - INTERVAL '1 HOUR'
        ) tr_all
        ORDER BY tr_all.source_identification ASC, tr_all.measurement_or_calculation_time DESC
    ) as tr
    ORDER BY tr.measurement_or_calculation_time DESC
)
