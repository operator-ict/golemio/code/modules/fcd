CREATE TABLE fcd_geos_90 (
	source_identification varchar(100) NOT NULL,
	oriented_route public.geometry NULL,
	CONSTRAINT fcd_geos_90_pkey PRIMARY KEY (source_identification)
);
COMMENT ON TABLE fcd_geos_90 IS 'lookup table for Praha Dopravni, data are prepared manually';

CREATE TABLE tmc_ltcze90_roads_wgs84 (
	geom public.geometry(multilinestring, 4326) NULL,
	cid int8 NULL,
	tabcd int8 NULL,
	lcd int8 NOT NULL,
	"class" varchar(254) NULL,
	tcd int8 NULL,
	stcd int8 NULL,
	roadnumber varchar(254) NULL,
	roadname varchar(254) NULL,
	firstname varchar(254) NULL,
	secondname varchar(254) NULL,
	area_ref int8 NULL,
	area_name varchar(254) NULL,
	CONSTRAINT tmc_ltcze90_roads_wgs84_pkey PRIMARY KEY (lcd)
);
COMMENT ON TABLE tmc_ltcze90_roads_wgs84 IS 'Data for this table are imported manually. Purpose of this table is to show static TMC road net in app Praha Dopravni.';