CREATE TABLE fcd_geos_90 (
	source_identification varchar(100) NOT NULL,
	oriented_route geometry NULL,
	CONSTRAINT fcd_geos_90_pkey PRIMARY KEY (source_identification)
);
COMMENT ON TABLE fcd_geos_90 IS 'lookup table for Praha Dopravni, data are prepared manually';
