CREATE OR REPLACE VIEW v_traffic_params_all as (
    select distinct on (source_identification, measurement_or_calculation_time) * from (
        select * from fcd.fcd_traff_params_part 
        where measurement_or_calculation_time = (select max(measurement_or_calculation_time) from fcd.fcd_traff_params_part)
        union all 
        select * from fcd.fcd_traff_params_regions 
        where measurement_or_calculation_time = (select max(measurement_or_calculation_time) from fcd.fcd_traff_params_regions) 
    ) as tr_all
    order by measurement_or_calculation_time desc, source_identification asc);

COMMENT ON VIEW v_traffic_params_all IS 'Pohled vracící aktuální data z fcd_traff_params_part a fcd_tarff_params_regions. Je použit v output gateway v modulu fcd.';
COMMENT ON TABLE fcd_traff_params_part IS 'Obsahuje fcd data z Hlavního města Praha a Středočeského kraje. Historická data se zachovávají.';
COMMENT ON TABLE fcd_traff_params_regions IS 'Obsahuje fcd data z krajů: Liberecký, Královehradecký, Pardubický, Vysočina, Jihočeský, Plzeňský, Karlovarský, Ústecký. Integrační engine pouští pravidelnou retenci dat.';
