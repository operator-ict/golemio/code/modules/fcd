CREATE TABLE fcd_traff_params_part (
	publication_time timestamptz NULL,
	source_identification varchar(50) NOT NULL,
	measurement_or_calculation_time timestamptz NOT NULL,
	predefined_location varchar(50) NOT NULL,
	version_of_predefined_location int4 NULL,
	traffic_level varchar(50) NOT NULL,
	queue_exists bool NULL,
	queue_length int4 NULL,
	from_point float4 NULL,
	to_point float4 NULL,
	data_quality float4 NULL,
	input_values int4 NULL,
	average_vehicle_speed int4 NULL,
	travel_time int4 NULL,
	free_flow_travel_time int4 NULL,
	free_flow_speed int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT fcd_traff_params_part_pk PRIMARY KEY (source_identification, measurement_or_calculation_time)
)  partition by range(measurement_or_calculation_time);

CREATE INDEX fcd_traff_params_part_time ON fcd_traff_params_part USING btree (measurement_or_calculation_time);

CREATE TABLE fcd_traff_params_part_min PARTITION OF fcd_traff_params_part
    FOR VALUES FROM (MINVALUE) TO ('2020-05-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2020m05 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2020-05-01'::timestamptz) TO ('2020-06-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2020m06 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2020-06-01'::timestamptz) TO ('2020-07-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2020m07 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2020-07-01'::timestamptz) TO ('2020-08-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2020m08 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2020-08-01'::timestamptz) TO ('2020-09-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2020m09 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2020-09-01'::timestamptz) TO ('2020-10-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2020m10 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2020-10-01'::timestamptz) TO ('2020-11-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2020m11 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2020-11-01'::timestamptz) TO ('2020-12-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2020m12 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2020-12-01'::timestamptz) TO ('2021-01-01'::timestamptz);

-- 2021
CREATE TABLE fcd_traff_params_part_y2021m01 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2021-01-01'::timestamptz) TO ('2021-02-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m02 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2021-02-01'::timestamptz) TO ('2021-03-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m03 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2021-03-01'::timestamptz) TO ('2021-04-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m04 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2021-04-01'::timestamptz) TO ('2021-05-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m05 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2021-05-01'::timestamptz) TO ('2021-06-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m06 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2021-06-01'::timestamptz) TO ('2021-07-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m07 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2021-07-01'::timestamptz) TO ('2021-08-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m08 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2021-08-01'::timestamptz) TO ('2021-09-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m09 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2021-09-01'::timestamptz) TO ('2021-10-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m10 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2021-10-01'::timestamptz) TO ('2021-11-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m11 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2021-11-01'::timestamptz) TO ('2021-12-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2021m12 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2021-12-01'::timestamptz) TO ('2022-01-01'::timestamptz);

-- 2022
CREATE TABLE fcd_traff_params_part_y2022m01 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2022-01-01'::timestamptz) TO ('2022-02-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m02 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2022-02-01'::timestamptz) TO ('2022-03-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m03 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2022-03-01'::timestamptz) TO ('2022-04-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m04 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2022-04-01'::timestamptz) TO ('2022-05-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m05 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2022-05-01'::timestamptz) TO ('2022-06-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m06 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2022-06-01'::timestamptz) TO ('2022-07-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m07 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2022-07-01'::timestamptz) TO ('2022-08-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m08 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2022-08-01'::timestamptz) TO ('2022-09-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m09 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2022-09-01'::timestamptz) TO ('2022-10-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m10 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2022-10-01'::timestamptz) TO ('2022-11-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m11 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2022-11-01'::timestamptz) TO ('2022-12-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2022m12 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2022-12-01'::timestamptz) TO ('2023-01-01'::timestamptz);


-- 2023
CREATE TABLE fcd_traff_params_part_y2023m01 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2023-01-01'::timestamptz) TO ('2023-02-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m02 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2023-02-01'::timestamptz) TO ('2023-03-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m03 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2023-03-01'::timestamptz) TO ('2023-04-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m04 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2023-04-01'::timestamptz) TO ('2023-05-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m05 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2023-05-01'::timestamptz) TO ('2023-06-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m06 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2023-06-01'::timestamptz) TO ('2023-07-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m07 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2023-07-01'::timestamptz) TO ('2023-08-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m08 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2023-08-01'::timestamptz) TO ('2023-09-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m09 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2023-09-01'::timestamptz) TO ('2023-10-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m10 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2023-10-01'::timestamptz) TO ('2023-11-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m11 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2023-11-01'::timestamptz) TO ('2023-12-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2023m12 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2023-12-01'::timestamptz) TO ('2024-01-01'::timestamptz);

-- 2024
CREATE TABLE fcd_traff_params_part_y2024m01 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2024-01-01'::timestamptz) TO ('2024-02-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m02 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2024-02-01'::timestamptz) TO ('2024-03-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m03 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2024-03-01'::timestamptz) TO ('2024-04-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m04 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2024-04-01'::timestamptz) TO ('2024-05-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m05 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2024-05-01'::timestamptz) TO ('2024-06-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m06 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2024-06-01'::timestamptz) TO ('2024-07-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m07 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2024-07-01'::timestamptz) TO ('2024-08-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m08 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2024-08-01'::timestamptz) TO ('2024-09-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m09 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2024-09-01'::timestamptz) TO ('2024-10-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m10 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2024-10-01'::timestamptz) TO ('2024-11-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m11 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2024-11-01'::timestamptz) TO ('2024-12-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2024m12 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2024-12-01'::timestamptz) TO ('2025-01-01'::timestamptz);

-- 2025
CREATE TABLE fcd_traff_params_part_y2025m01 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2025-01-01'::timestamptz) TO ('2025-02-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m02 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2025-02-01'::timestamptz) TO ('2025-03-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m03 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2025-03-01'::timestamptz) TO ('2025-04-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m04 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2025-04-01'::timestamptz) TO ('2025-05-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m05 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2025-05-01'::timestamptz) TO ('2025-06-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m06 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2025-06-01'::timestamptz) TO ('2025-07-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m07 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2025-07-01'::timestamptz) TO ('2025-08-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m08 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2025-08-01'::timestamptz) TO ('2025-09-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m09 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2025-09-01'::timestamptz) TO ('2025-10-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m10 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2025-10-01'::timestamptz) TO ('2025-11-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m11 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2025-11-01'::timestamptz) TO ('2025-12-01'::timestamptz);

CREATE TABLE fcd_traff_params_part_y2025m12 PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2025-12-01'::timestamptz) TO ('2026-01-01'::timestamptz);
-- > 25

CREATE TABLE fcd_traff_params_part_y2026mxx PARTITION OF fcd_traff_params_part
    FOR VALUES FROM ('2026-01-01'::timestamptz) TO (MAXVALUE);

