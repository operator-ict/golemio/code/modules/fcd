CREATE TABLE tmc_ltcze90_roads_wgs84 (
    geom geometry(MultiLineString,4326),
    cid bigint,
    tabcd bigint,
    lcd bigint PRIMARY KEY,
    class character varying(254),
    tcd bigint,
    stcd bigint,
    roadnumber character varying(254),
    roadname character varying(254),
    firstname character varying(254),
    secondname character varying(254),
    area_ref bigint,
    area_name character varying(254)
);
COMMENT ON TABLE tmc_ltcze90_roads_wgs84 IS 'Data for this table are imported manually. Purpose of this table is to show static TMC road net in app Praha Dopravni.';