CREATE TABLE fcd_traff_params_regions (
	publication_time timestamptz NULL,
	source_identification varchar(50) NOT NULL,
	measurement_or_calculation_time timestamptz NOT NULL,
	predefined_location varchar(50) NOT NULL,
	version_of_predefined_location int4 NULL,
	traffic_level varchar(50) NOT NULL,
	queue_exists bool NULL,
	queue_length int4 NULL,
	from_point float4 NULL,
	to_point float4 NULL,
	data_quality float4 NULL,
	input_values int4 NULL,
	average_vehicle_speed int4 NULL,
	travel_time int4 NULL,
	free_flow_travel_time int4 NULL,
	free_flow_speed int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT fcd_traff_params_regions_pk PRIMARY KEY (source_identification, measurement_or_calculation_time)
);
CREATE INDEX fcd_traff_params_regions_time ON ONLY fcd_traff_params_regions USING btree (measurement_or_calculation_time);

create view v_traffic_params_all as 
	select * from fcd_traff_params_part
	union all 
	select * from fcd_traff_params_regions;

CREATE PROCEDURE regions_data_retention(dataretentionminutes integer)
 LANGUAGE plpgsql
AS $procedure$    
	begin
		delete from fcd.fcd_traff_params_regions where measurement_or_calculation_time < (NOW() - (dataretentionminutes || ' minutes')::interval);
	end;
$procedure$
;