INSERT INTO fcd_traff_params_part
(publication_time, source_identification, measurement_or_calculation_time, predefined_location, version_of_predefined_location, traffic_level, queue_exists, queue_length, from_point, to_point, data_quality, input_values, average_vehicle_speed, travel_time, free_flow_travel_time, free_flow_speed, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by)
VALUES('2021-09-21 01:36:59.000', 'TS10125T10123', '2021-09-21 01:36:01.000', 'TS10125T10123', 2, 'level2', false, NULL, NULL, NULL, 0.5, 1, 35, 31, 23, 46, NULL, '2021-10-18 11:01:16.534', NULL, NULL, '2021-10-18 11:01:16.534', NULL);
INSERT INTO fcd_traff_params_part
(publication_time, source_identification, measurement_or_calculation_time, predefined_location, version_of_predefined_location, traffic_level, queue_exists, queue_length, from_point, to_point, data_quality, input_values, average_vehicle_speed, travel_time, free_flow_travel_time, free_flow_speed, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by)
VALUES('2021-09-21 01:36:59.000', 'TS10127T19629', '2021-09-21 01:36:01.000', 'TS10127T19629', 2, 'level1', false, NULL, NULL, NULL, 0.5, 1, 47, 26, 25, 48, NULL, '2021-10-18 11:01:16.534', NULL, NULL, '2021-10-18 11:01:16.534', NULL);
INSERT INTO fcd_traff_params_part
(publication_time, source_identification, measurement_or_calculation_time, predefined_location, version_of_predefined_location, traffic_level, queue_exists, queue_length, from_point, to_point, data_quality, input_values, average_vehicle_speed, travel_time, free_flow_travel_time, free_flow_speed, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by)
VALUES('2021-09-21 01:36:59.000', 'TS10182T24828', '2021-09-21 01:36:01.000', 'TS10182T24828', 2, 'level1', false, NULL, NULL, NULL, 0.833, 3, 91, 87, 106, 76, NULL, '2021-10-18 11:01:16.534', NULL, NULL, '2021-10-18 11:01:16.534', NULL);


DELETE from traffic.rsd_tmc_osm_mapping;

INSERT INTO traffic.rsd_tmc_osm_mapping (lt_start,lt_end,osm_path) VALUES
	 (10125,10123,'[1837041102, 8418051939, 7881860434, 1837022954, 1837022956, 1837022966, 977504781, 8404188228, 5216395186, 2674815365, 977504746, 8402974140, 1017997574, 1017997516, 1017997560, 1017997515, 8403075909, 8403127730, 8403075911, 8403075912, 8403127735]'),
	 (10127,19629,'[29381030, 29403193, 338604070, 29381028, 338604072, 29403192, 2681371748, 1138616634, 2681371747, 308815174,1138624651, 29403191]');