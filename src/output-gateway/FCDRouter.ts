import { FcdContainer } from "#og/ioc/Di";
import { OutputFloatingCarDataTransformation } from "#og/transformations/OutputFloatingCarDataTransformation";
import { IFloatingCarWithOsmPathDataModel } from "#sch/interfaces/FloatingCarDataInterface";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { BaseRouter } from "@golemio/core/dist/output-gateway/routes/BaseRouter";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { query } from "@golemio/core/dist/shared/express-validator";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import {
    FloatingCarDataModel,
    FloatingCarExtendedDataModel,
    FloatingCarWithOsmPathDataModel,
    FloatingCarWithOsmPathExtendedDataModel,
} from "./models";
export class FCDRouter extends BaseRouter {
    protected cacheHeaderMiddleware: CacheHeaderMiddleware;
    public constructor(
        protected floatingCarDataModel: FloatingCarDataModel,
        protected floatingCarWithOsmPathDataModel: FloatingCarWithOsmPathDataModel,
        protected floatingCarExtendedDataModel: FloatingCarExtendedDataModel,
        protected floatingCarWithOsmPathExtendedDataModel: FloatingCarWithOsmPathExtendedDataModel,
        private outputFloatingCarDataTransformation: OutputFloatingCarDataTransformation
    ) {
        super();

        this.cacheHeaderMiddleware = FcdContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);

        this.router.get(
            "/info",
            [
                query("osmPath").optional().not().isEmpty({ ignore_whitespace: true }).not().isArray(),
                query("locationId").optional().not().isEmpty({ ignore_whitespace: true }).not().isArray(),
                query("minutesBefore").optional().isInt({ min: 1, max: 60 }).not().isArray(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("FCDRouter"),
            this.cacheHeaderMiddleware.getMiddleware(3 * 60, 60),
            this.GetFloatingCarData
        );
    }

    public GetFloatingCarData = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const isRequestedPathOsm = (req.query.osmPath as string) == "true";
            const minutesBefore = req.query.minutesBefore ? parseInt(req.query.minutesBefore as string, 10) : undefined;

            const options = {
                locationId: req.query.locationId as string,
                limit: req.query.limit ? +req.query.limit : undefined,
                offset: req.query.offset ? +req.query.offset : undefined,
                minutesBefore,
                isRequestedPathOsm,
            };

            let dataFromDb: IFloatingCarWithOsmPathDataModel[] = [];

            if (isRequestedPathOsm) {
                dataFromDb = minutesBefore
                    ? await this.floatingCarWithOsmPathExtendedDataModel.GetAll(options)
                    : await this.floatingCarWithOsmPathDataModel.GetAll(options);
            } else {
                dataFromDb = minutesBefore
                    ? await this.floatingCarExtendedDataModel.GetAll(options)
                    : await this.floatingCarDataModel.GetAll(options);
            }
            if (!dataFromDb?.length) {
                throw new GeneralError("No data found", "FCDRouter", undefined, 404);
            }

            const transformedData = this.outputFloatingCarDataTransformation.transform(dataFromDb, isRequestedPathOsm);
            res.status(200).send(transformedData);
        } catch (err) {
            next(err);
        }
    };
}

const fcdRouter: Router = new FCDRouter(
    new FloatingCarDataModel(),
    new FloatingCarWithOsmPathDataModel(),
    new FloatingCarExtendedDataModel(),
    new FloatingCarWithOsmPathExtendedDataModel(),
    new OutputFloatingCarDataTransformation()
).router;

export { fcdRouter };
