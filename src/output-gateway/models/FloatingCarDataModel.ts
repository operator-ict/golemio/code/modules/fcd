import { FCD } from "#sch/index";
import { IFloatingCarDataModel } from "#sch/interfaces/FloatingCarDataInterface";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import Sequelize from "@golemio/core/dist/shared/sequelize";

export class FloatingCarDataModel extends SequelizeModel {
    public constructor() {
        super(FCD.fcd_info.name, "v_traffic_params_all", FCD.fcd_info.outputSequelizeAttributes, {
            schema: FCD.pgSchema,
        });
    }

    public GetAll = async (
        options: {
            locationId?: string;
            limit?: number;
            offset?: number;
        } = {}
    ): Promise<IFloatingCarDataModel[]> => {
        const { locationId, limit, offset } = options;
        try {
            const Op = Sequelize.Op;
            const where = {
                [Op.and]: [{}],
            };

            if (locationId) {
                where[Op.and].push({
                    predefined_location: locationId,
                });
            }

            return await this.sequelizeModel.findAll({
                limit,
                offset,
                raw: true,
                where,
            });
        } catch (err) {
            throw new GeneralError(`GetAll method error: ${err.message}`, "FloatingCarDataModel", err, 500);
        }
    };

    public GetOne = async (): Promise<object | null> => {
        throw new GeneralError("Not implemented", "FloatingCarDataModel");
    };
}
