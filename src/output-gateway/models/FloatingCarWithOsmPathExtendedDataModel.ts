import { FCD } from "#sch/index";
import { IFloatingCarWithOsmPathDataModel } from "#sch/interfaces/FloatingCarDataInterface";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import Sequelize from "@golemio/core/dist/shared/sequelize";

export class FloatingCarWithOsmPathExtendedDataModel extends SequelizeModel {
    public constructor() {
        super(FCD.fcd_info.name, "v_traffic_params_last_hour_osm_path", FCD.fcd_info.outputSequelizeAttributesWithOsmPath, {
            schema: FCD.pgSchema,
        });
    }

    public GetAll = async (
        options: {
            locationId?: string;
            minutesBefore?: number;
            limit?: number;
            offset?: number;
        } = {}
    ): Promise<IFloatingCarWithOsmPathDataModel[]> => {
        const { locationId, minutesBefore, limit, offset } = options;
        try {
            const Op = Sequelize.Op;
            const where = {
                [Op.and]: [{}],
            };

            if (locationId) {
                where[Op.and].push({
                    predefined_location: locationId,
                });
            }

            if (minutesBefore) {
                where[Op.and].push({
                    measurement_or_calculation_time: {
                        [Sequelize.Op.gte]: Sequelize.literal(`NOW() - INTERVAL '${minutesBefore} minutes'`),
                    },
                });
            }

            return await this.sequelizeModel.findAll({
                limit,
                offset,
                raw: true,
                where,
            });
        } catch (err) {
            throw new GeneralError(`GetAll method error: ${err.message}`, "FloatingCarExtendedDataModel", err, 500);
        }
    };

    public GetOne = async (): Promise<object | null> => {
        throw new GeneralError("Not implemented", "FloatingCarExtendedDataModel");
    };
}
