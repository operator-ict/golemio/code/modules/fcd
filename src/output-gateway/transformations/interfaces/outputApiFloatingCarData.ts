import { IOutputApiFloatingCarDataElaboratedData } from "#sch/interfaces/FloatingCarDataInterface";

export interface IOutputApiFloatingCarData {
    modelBaseVersion: string;
    payloadPublicationLight: {
        lang: string;
        publicationTime: string;
        publicationCreator: {
            country: string;
            nationalIdentifier: string;
        };
        elaboratedData: IOutputApiFloatingCarDataElaboratedData[];
    };
}
