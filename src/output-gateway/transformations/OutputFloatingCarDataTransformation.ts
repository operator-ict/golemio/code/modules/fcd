import { FCD } from "#sch";
import {
    IFloatingCarWithOsmPathDataModel,
    IOutputApiFloatingCarDataElaboratedData,
} from "#sch/interfaces/FloatingCarDataInterface";
import { dateTime } from "@golemio/core/dist/helpers";
import { IOutputApiFloatingCarData } from "./interfaces/outputApiFloatingCarData";

export class OutputFloatingCarDataTransformation {
    name: string;
    private latestPublicationTime: Date;

    constructor() {
        this.name = FCD.fcd_info.name;
        this.latestPublicationTime = new Date();
    }

    public transform = (data: IFloatingCarWithOsmPathDataModel[], isRequestedPathOsm: boolean): IOutputApiFloatingCarData => {
        let elaboratedData: IOutputApiFloatingCarDataElaboratedData[] = [];
        this.latestPublicationTime = new Date(data[0].measurement_or_calculation_time);
        for (const item of data) {
            elaboratedData.push(this.transformElement(item, isRequestedPathOsm));
        }
        return {
            modelBaseVersion: "3",
            payloadPublicationLight: {
                lang: "cz",
                publicationTime: dateTime(this.latestPublicationTime).setTimeZone("Europe/Prague").toISOString(),
                publicationCreator: {
                    country: "cz",
                    nationalIdentifier: "ŘSD",
                },
                elaboratedData,
            },
        };
    };

    public transformElement = (item: IFloatingCarWithOsmPathDataModel, isRequestedPathOsm: boolean) => {
        const itemMeasurementTime = new Date(item.measurement_or_calculation_time);
        const itemResultObject: IOutputApiFloatingCarDataElaboratedData = {
            measurementOrCalculationTime: dateTime(itemMeasurementTime).setTimeZone("Europe/Prague").toISOString(),
            sourceIdentification: item.source_identification,
            supplierCalculatedDataQuality: item.data_quality.toString(),
            numberOfInputValuesUsed: item.input_values.toString(),
            predefinedLocationId: item.predefined_location,
            trafficLevel: item.traffic_level,
            averageVehicleSpeed: item.average_vehicle_speed.toString(),
            travelTime: item.travel_time.toString(),
            freeFlowTravelTime: item.free_flow_travel_time.toString(),
            freeFlowSpeed: item.free_flow_speed.toString(),
        };

        if (item.queue_exists) {
            itemResultObject.queueExists = item.queue_exists.toString();
        }

        if (item.queue_length) {
            itemResultObject.queueLength = item.queue_length.toString();
        }

        if (item.from_point) {
            itemResultObject.fromPoint = item.from_point.toString();
        }

        if (item.to_point) {
            itemResultObject.toPoint = item.to_point.toString();
        }
        if (isRequestedPathOsm) {
            itemResultObject.osmPath = item.osm_path ? JSON.parse(item.osm_path) : null;
            if (itemMeasurementTime > this.latestPublicationTime) {
                this.latestPublicationTime = itemMeasurementTime;
            }
        }
        return itemResultObject;
    };
}
