import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/Di";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";

//#region Initialization
const fcdContainer: DependencyContainer = OutputGatewayContainer.createChildContainer();
//#endregion

export { fcdContainer as FcdContainer };
