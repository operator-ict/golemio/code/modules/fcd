const ModuleContainerToken = {
    FloatingCarDataTransformation: Symbol(),
    SaveFloatingCarDataTask: Symbol(),
    SaveRegionsDataTask: Symbol(),
    RegionsDataRetentionTask: Symbol(),
    FloatingCarDataRegionsRepository: Symbol(),
    FloatingCarDataRepository: Symbol(),
    lteCze90PointsChecker: Symbol(),
};

export { ModuleContainerToken };
