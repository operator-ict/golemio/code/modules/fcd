import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { FloatingCarDataTransformation } from "#ie/transformations";
import { LteCze90PointsChecker } from "#ie/helpers/LteCze90PointsChecker";
import { ModuleContainerToken } from "./ModuleContainer";
import { FloatingCarDataRegionsRepository } from "#ie/repositories/fcd/FloatingCarDataRegionsRepository";
import { FloatingCarDataRepository } from "#ie/repositories/fcd/FloatingCarDataRepository";
import { RegionsDataRetentionTask, SaveFloatingCarDataTask, SaveRegionsDataTask } from "#ie/workers/tasks";

//#region Initialization
const FCDContainer: DependencyContainer = IntegrationEngineContainer.createChildContainer();
//#endregion

//#region Repositories
FCDContainer.register(ModuleContainerToken.FloatingCarDataRegionsRepository, FloatingCarDataRegionsRepository);
FCDContainer.register(ModuleContainerToken.FloatingCarDataRepository, FloatingCarDataRepository);
//#endregion

//#region Transformations
FCDContainer.register(ModuleContainerToken.FloatingCarDataTransformation, FloatingCarDataTransformation);
//#endregion

//#region Tasks
FCDContainer.register(ModuleContainerToken.RegionsDataRetentionTask, RegionsDataRetentionTask);
FCDContainer.register(ModuleContainerToken.SaveFloatingCarDataTask, SaveFloatingCarDataTask);
FCDContainer.register(ModuleContainerToken.SaveRegionsDataTask, SaveRegionsDataTask);
//#endregion

//#region Helpers
FCDContainer.register(ModuleContainerToken.lteCze90PointsChecker, LteCze90PointsChecker);
//#endregion

export { FCDContainer };
