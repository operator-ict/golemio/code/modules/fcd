import { FloatingCarDataTransformation, IFloatingCarData } from "#ie";
import { FCD } from "#sch";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainer";
import { FloatingCarDataRepository } from "#ie/repositories/fcd/FloatingCarDataRepository";
import { AbstractTaskJsonSchema } from "@golemio/core/dist/integration-engine/workers/AbstractTaskJsonSchema";
import { floatingCarDataJsonSchema } from "#ie/schemas/floatingCarData";

@injectable()
export class SaveFloatingCarDataTask extends AbstractTaskJsonSchema<IFloatingCarData> {
    public readonly queueName = "saveFloatingCarData";

    public readonly queueTtl = 59 * 60 * 1000;
    protected schema = new JSONSchemaValidator("floatingCarDataValidation", floatingCarDataJsonSchema);

    constructor(
        @inject(ModuleContainerToken.FloatingCarDataTransformation)
        private floatingCarDataTransformation: FloatingCarDataTransformation,
        @inject(ModuleContainerToken.FloatingCarDataRepository)
        private floatingCarDataRepository: FloatingCarDataRepository
    ) {
        super(FCD.name.toLowerCase());
    }

    protected async execute(data: IFloatingCarData): Promise<void> {
        try {
            const transformedData = await this.floatingCarDataTransformation.transform(data);
            await this.floatingCarDataRepository.save(transformedData);
        } catch (error) {
            throw new GeneralError("Error while processing Floating Car Data.", "SaveFloatingCarDataTask", error, 422);
        }
    }
}
