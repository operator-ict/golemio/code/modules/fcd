import { LteCze90PointsChecker } from "#ie/helpers/LteCze90PointsChecker";
import { FloatingCarDataTransformation, IFloatingCarData } from "#ie";
import { FCD } from "#sch";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { FloatingCarDataRegionsRepository } from "#ie/repositories/fcd/FloatingCarDataRegionsRepository";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainer";
import { AbstractTaskJsonSchema } from "@golemio/core/dist/integration-engine/workers/AbstractTaskJsonSchema";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { SchemaDefinitions } from "src";

@injectable()
export class SaveRegionsDataTask extends AbstractTaskJsonSchema<IFloatingCarData> {
    public readonly queueName = "saveRegionsData";
    public readonly queueTtl = 59 * 60 * 1000;
    protected schema = new JSONSchemaValidator("floatingCarDataValidation", SchemaDefinitions.FCD.datasourceSchema);

    constructor(
        @inject(ModuleContainerToken.FloatingCarDataTransformation)
        private floatingCarDataTransformation: FloatingCarDataTransformation,
        @inject(ModuleContainerToken.lteCze90PointsChecker)
        private lteCze90PointsChecker: LteCze90PointsChecker,
        @inject(ModuleContainerToken.FloatingCarDataRegionsRepository)
        private floatingCarDataRegionsModel: FloatingCarDataRegionsRepository
    ) {
        super(FCD.name.toLowerCase());
    }

    protected async execute(data: IFloatingCarData): Promise<void> {
        try {
            const transformedData = await this.floatingCarDataTransformation.transform(data);
            await this.lteCze90PointsChecker.initialize();
            const filteredData = transformedData.filter((element) =>
                this.lteCze90PointsChecker.shouldSave(element.source_identification)
            );

            await this.floatingCarDataRegionsModel.save(filteredData);
        } catch (error) {
            throw new GeneralError("Error while processing Floating Car Data for regions.", "SaveRegionsDataTask", error, 422);
        }
    }
}
