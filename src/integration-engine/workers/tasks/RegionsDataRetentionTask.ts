import { FCD } from "#sch";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { QueryTypes } from "@golemio/core/dist/shared/sequelize";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

@injectable()
export class RegionsDataRetentionTask extends AbstractEmptyTask {
    public readonly queueName = "regionsDataRetention";
    public readonly queueTtl = 59 * 60 * 1000;
    private DATA_RETENTION_IN_MINUTES = 2880; // 2 days

    constructor(@inject(CoreToken.PostgresConnector) private postgresConnector: IDatabaseConnector) {
        super(FCD.name.toLowerCase());
    }
    protected async execute(): Promise<void> {
        try {
            const sequelize = await this.postgresConnector.connect();
            await sequelize.query(`CALL ${FCD.pgSchema}.regions_data_retention(${this.DATA_RETENTION_IN_MINUTES});`, {
                plain: true,
                type: QueryTypes.SELECT,
            });
        } catch (error) {
            throw new GeneralError("Error while processing Regions Data Retention Task", "RegionsDataRetentionTask", error, 422);
        }
    }
}
