import { AbstractTask, AbstractWorker } from "@golemio/core/dist/integration-engine";
import { SaveFloatingCarDataTask, SaveRegionsDataTask, RegionsDataRetentionTask } from "./tasks";
import { FCDContainer } from "#ie/ioc/Di";
import { AbstractTaskJsonSchema } from "@golemio/core/dist/integration-engine/workers/AbstractTaskJsonSchema";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainer";

export class FCDWorker extends AbstractWorker {
    protected readonly name = "Fcd";
    constructor() {
        super();
        this.registerTask(FCDContainer.resolve<SaveFloatingCarDataTask>(ModuleContainerToken.SaveFloatingCarDataTask));
        this.registerTask(FCDContainer.resolve<SaveRegionsDataTask>(ModuleContainerToken.SaveRegionsDataTask));
        this.registerTask(FCDContainer.resolve<RegionsDataRetentionTask>(ModuleContainerToken.RegionsDataRetentionTask));
    }

    public registerTask = (task: AbstractTaskJsonSchema<any> | AbstractTask<any>): void => {
        super.registerTask(task);
        task.queuePrefix = this.getQueuePrefix();
    };
}
