import { LteCze90PointsSjtskInAreaRepository } from "@golemio/traffic-common/dist/integration-engine/repositories/LteCze90PointsSjtskInAreaRepository";

export class LteCze90PointsChecker {
    private filtrationLookup: LteCze90PointsSjtskInAreaRepository;
    private relevantLcds: number[] = [];
    private SOURCE_ID_REGEX = /^TS(?<secondaryPoint>\d+)T(?<primaryPoint>\d+)$/;

    constructor() {
        this.filtrationLookup = new LteCze90PointsSjtskInAreaRepository();
    }

    public async initialize(): Promise<void> {
        this.relevantLcds = (await this.filtrationLookup.getFiltrationData()).map((element) => element.lcd);
    }

    public shouldSave(sourceIdentification: string | undefined): boolean {
        const regexResult = sourceIdentification ? this.SOURCE_ID_REGEX.exec(sourceIdentification) : undefined;

        const primaryPointSpecificLocation = regexResult?.groups?.primaryPoint;
        const secondaryPointSpecificLocation = regexResult?.groups?.secondaryPoint;

        if (
            primaryPointSpecificLocation === undefined ||
            primaryPointSpecificLocation === null ||
            secondaryPointSpecificLocation === undefined ||
            secondaryPointSpecificLocation === null
        ) {
            return false;
        }

        return (
            this.relevantLcds.includes(Number(primaryPointSpecificLocation)) ||
            this.relevantLcds.includes(Number(secondaryPointSpecificLocation))
        );
    }
}
