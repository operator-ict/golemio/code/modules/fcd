import { IFloatingCarDataModel } from "#sch/interfaces/FloatingCarDataInterface";

export interface IFloatingCarData {
    $: {
        xmlns: string;
        "xmlns:xsi": string;
        modelBaseVersion: string;
    };
    exchange: {
        supplierIdentification: {
            country: string;
            nationalIdentifier: string;
        };
    };
    payloadPublication: {
        $: {
            "xsi:type": string;
            lang: string;
        };
        publicationTime: string;
        publicationCreator: Record<string, any>;
        headerInformation: Record<string, any>;
        elaboratedData: IElaboratedData[];
    };
}

export interface IElaboratedData {
    source: {
        sourceIdentification: string;
    };
    basicData: IBasicData;
}

export interface IBasicData {
    $: {
        "xsi:type": string;
    };
    measurementOrCalculationTime: string;
    pertinentLocation: IPertinentLocation;
    trafficStatusExtension?: ITrafficStatusExtension;
    travelTime?: ITravelTime;
    freeFlowTravelTime?: IFreeFlowTravelTime;
    freeFlowSpeed?: IFreeFlowSpeed;
    averageVehicleSpeed?: IAverageVehicleSpeed;
}

export interface IPertinentLocation {
    $: {
        "xsi:type": string;
    };
    predefinedLocationReference: {
        $: {
            targetClass: string;
            id: string;
            version: string;
        };
    };
}

interface ITrafficStatusExtension {
    ndicFcdExtension: {
        trafficLevel: {
            $: {
                supplierCalculatedDataQuality: string;
                numberOfInputValuesUsed: string;
            };
            trafficLevelValue: string;
        };
        queueInformation?: {
            queue: {
                queueLength: string;
                fromPoint: string;
                toPoint: string;
            };
        };
    };
}

interface ITravelTime {
    $: {
        supplierCalculatedDataQuality: string;
        numberOfInputValuesUsed: string;
    };
    duration: string;
}

interface IFreeFlowTravelTime {
    $: {
        supplierCalculatedDataQuality: string;
        numberOfInputValuesUsed: string;
    };
    duration: string;
}

interface IFreeFlowSpeed {
    $: {
        supplierCalculatedDataQuality: string;
        numberOfInputValuesUsed: string;
    };
    speed: string;
}

interface IAverageVehicleSpeed {
    $: {
        supplierCalculatedDataQuality: string;
        numberOfInputValuesUsed: string;
    };
    speed: string;
}
export type IFloatingCarDataModelElaboratedPart = Omit<IFloatingCarDataModel, "publication_time">;
