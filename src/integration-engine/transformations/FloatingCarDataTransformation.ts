import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { FCD } from "#sch";
import { IFloatingCarData, IElaboratedData, IFloatingCarDataModelElaboratedPart } from "#ie";
import { IFloatingCarDataModel } from "#sch/interfaces/FloatingCarDataInterface";

export class FloatingCarDataTransformation extends BaseTransformation implements ITransformation {
    name: string;

    constructor() {
        super();
        this.name = FCD.fcd_info.name;
    }

    public transform = async (data: IFloatingCarData): Promise<IFloatingCarDataModel[]> => {
        const collectedElaboratedDataArr: IElaboratedData[][] = await this.collectElaboratedData(data);

        const result: IFloatingCarDataModel[] = [];

        for (const elaboratedDataGroup of collectedElaboratedDataArr) {
            const elaboratedDataTransformed: IFloatingCarDataModelElaboratedPart = await this.transformElement(
                elaboratedDataGroup
            );
            result.push({ ...{ publication_time: data.payloadPublication.publicationTime }, ...elaboratedDataTransformed });
        }
        return result;
    };

    protected transformElement = async (elaboratedDataGroup: IElaboratedData[]): Promise<IFloatingCarDataModelElaboratedPart> => {
        // 0 or "" will be replaced by a value from one of the array objects
        let source_identification = "",
            measurement_or_calculation_time = "",
            predefined_location = "",
            version_of_predefined_location = 0,
            traffic_level = "",
            data_quality = 0,
            input_values = 0,
            queue_exists,
            queue_length,
            from_point,
            to_point,
            average_vehicle_speed = 0,
            travel_time = 0,
            free_flow_travel_time = 0,
            free_flow_speed = 0;

        for (const item of elaboratedDataGroup) {
            source_identification = item.source?.sourceIdentification;
            measurement_or_calculation_time = item.basicData.measurementOrCalculationTime;
            predefined_location = item.basicData.pertinentLocation.predefinedLocationReference.$.id;
            version_of_predefined_location = +item.basicData.pertinentLocation.predefinedLocationReference.$.version;

            if (item.basicData.trafficStatusExtension) {
                traffic_level = item.basicData.trafficStatusExtension.ndicFcdExtension.trafficLevel.trafficLevelValue;
                data_quality =
                    +item.basicData.trafficStatusExtension.ndicFcdExtension.trafficLevel.$.supplierCalculatedDataQuality;
                input_values = +item.basicData.trafficStatusExtension.ndicFcdExtension.trafficLevel.$.numberOfInputValuesUsed;

                queue_exists = Boolean(item.basicData.trafficStatusExtension.ndicFcdExtension.queueInformation);
                if (item.basicData.trafficStatusExtension.ndicFcdExtension.queueInformation) {
                    queue_length = +item.basicData.trafficStatusExtension.ndicFcdExtension.queueInformation.queue.queueLength;
                    from_point = +item.basicData.trafficStatusExtension.ndicFcdExtension.queueInformation.queue.fromPoint;
                    to_point = +item.basicData.trafficStatusExtension.ndicFcdExtension.queueInformation.queue.toPoint;
                }
            }

            if (item.basicData.averageVehicleSpeed) {
                average_vehicle_speed = +item.basicData.averageVehicleSpeed.speed;
            }
            if (item.basicData.travelTime) {
                travel_time = +item.basicData.travelTime.duration;
            }
            if (item.basicData.freeFlowTravelTime) {
                free_flow_travel_time = +item.basicData.freeFlowTravelTime.duration;
            }
            if (item.basicData.freeFlowSpeed) {
                free_flow_speed = +item.basicData.freeFlowSpeed.speed;
            }
        }
        return {
            source_identification,
            measurement_or_calculation_time,
            predefined_location,
            version_of_predefined_location,
            traffic_level,
            data_quality,
            input_values,
            queue_exists,
            queue_length,
            from_point,
            to_point,
            average_vehicle_speed,
            travel_time,
            free_flow_travel_time,
            free_flow_speed,
        };
    };

    protected collectElaboratedData = async (data: IFloatingCarData): Promise<IElaboratedData[][]> => {
        const elaboratedDataGrouped = this.groupByProperties(data.payloadPublication.elaboratedData, (item: IElaboratedData) => {
            return [item.source.sourceIdentification, item.basicData.measurementOrCalculationTime];
        });
        return elaboratedDataGrouped;
    };

    private groupByProperties = (elaboratedDataArr: IElaboratedData[], f: any) => {
        let groups: any = {};
        elaboratedDataArr.forEach((o: IElaboratedData) => {
            let group = JSON.stringify(f(o));
            groups[group] = groups[group] || [];
            groups[group].push(o);
        });
        return Object.keys(groups).map((group) => {
            return groups[group];
        });
    };
}
