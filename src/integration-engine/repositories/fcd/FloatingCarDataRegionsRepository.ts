import { FCD } from "#sch";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class FloatingCarDataRegionsRepository extends PostgresModel implements IModel {
    constructor(@inject(CoreToken.PostgresConnector) private databaseConnector: IDatabaseConnector) {
        super(
            FCD.fcd_info.name + "Model",
            {
                outputSequelizeAttributes: FCD.fcd_info.outputSequelizeAttributes,
                pgSchema: FCD.pgSchema,
                pgTableName: "fcd_traff_params_regions",
                savingType: "insertOnly",
            },
            new JSONSchemaValidator(FCD.fcd_info.name + "PgModelValidator", FCD.fcd_info.outputSchema)
        );
    }
}
