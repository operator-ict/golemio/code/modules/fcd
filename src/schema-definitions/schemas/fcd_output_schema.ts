import Sequelize from "@golemio/core/dist/shared/sequelize";

export const outputFloatingCarDataSchema = {
    type: "array",
    items: {
        type: "object",
        required: [
            "publication_time",
            "source_identification",
            "measurement_or_calculation_time",
            "predefined_location",
            "version_of_predefined_location",
            "traffic_level",
            "data_quality",
            "input_values",
            "average_vehicle_speed",
            "travel_time",
            "free_flow_travel_time",
            "free_flow_speed",
        ],
        additionalProperties: false,
        properties: {
            publication_time: { type: "string" },
            source_identification: { type: "string" },
            measurement_or_calculation_time: { type: "string" },
            predefined_location: { type: "string" },
            version_of_predefined_location: { type: "number" },
            traffic_level: { type: "string" },

            // ⬐ Not required
            queue_exists: { type: "boolean" },
            queue_length: { type: "number" },
            from_point: { type: "number" },
            to_point: { type: "number" },

            data_quality: { type: "number" },
            input_values: { type: "number" },
            average_vehicle_speed: { type: "number" },
            travel_time: { type: "number" },
            free_flow_travel_time: { type: "number" },
            free_flow_speed: { type: "number" },
        },
    },
};

export const outputFloatingCarDataSDMA: Sequelize.ModelAttributes<any> = {
    publication_time: { type: Sequelize.DATE },
    source_identification: { primaryKey: true, type: Sequelize.STRING(50) },
    measurement_or_calculation_time: { primaryKey: true, type: Sequelize.DATE },
    predefined_location: { type: Sequelize.STRING(50) },
    version_of_predefined_location: { type: Sequelize.INTEGER },
    traffic_level: { type: Sequelize.STRING(50) },

    // ⬐ Not required
    queue_exists: { type: Sequelize.BOOLEAN },
    queue_length: { type: Sequelize.INTEGER },
    from_point: { type: Sequelize.REAL },
    to_point: { type: Sequelize.REAL },

    data_quality: { type: Sequelize.REAL },
    input_values: { type: Sequelize.INTEGER },
    average_vehicle_speed: { type: Sequelize.INTEGER },
    travel_time: { type: Sequelize.INTEGER },
    free_flow_travel_time: { type: Sequelize.INTEGER },
    free_flow_speed: { type: Sequelize.INTEGER },

    // ⬐ Audit fields
    create_batch_id: { type: Sequelize.BIGINT },
    created_at: { type: Sequelize.DATE },
    created_by: { type: Sequelize.STRING(150) },
    update_batch_id: { type: Sequelize.BIGINT },
    updated_at: { type: Sequelize.DATE },
    updated_by: { type: Sequelize.STRING(150) },
};

export const outputFloatingCarDataSDMAWithOsmPath: Sequelize.ModelAttributes<any> = {
    ...outputFloatingCarDataSDMA,
    osm_path: { type: Sequelize.STRING },
};
