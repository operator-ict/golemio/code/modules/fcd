export const datasourceFloatingCarDataSchema = {
    title: "Floating Car Data schema",
    type: "object",
    properties: {
        d2LogicalModel: {
            type: "object",
            properties: {
                $: {
                    type: "object",
                    properties: {
                        modelBaseVersion: {
                            type: "string",
                            enum: ["2"],
                        },
                    },
                    required: ["modelBaseVersion"],
                    additionalProperties: true,
                },
                exchange: {
                    $ref: "#/definitions/Exchange",
                },
                payloadPublication: {
                    $ref: "#/definitions/PayloadPublication",
                },
            },
            required: ["exchange", "payloadPublication"],
            additionalProperties: false,
        },
    },
    required: ["d2LogicalModel"],
    additionalProperties: false,

    definitions: {
        Exchange: {
            type: "object",
            properties: {
                supplierIdentification: {
                    $ref: "#/definitions/InternationalIdentifier",
                },
                exchangeExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["supplierIdentification"],
        },
        _ExtensionType: {
            type: "object",
        },
        InternationalIdentifier: {
            type: "object",
            properties: {
                country: {
                    type: "string",
                },
                nationalIdentifier: {
                    type: "string",
                },
                internationalIdentifierExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["country", "nationalIdentifier"],
            additionalProperties: false,
        },
        //----------------- PayloadPublication ----------------
        PayloadPublication: {
            type: "object",
            properties: {
                $: {
                    type: "object",
                    properties: {
                        "xsi:type": {
                            type: "string",
                            enum: ["ElaboratedDataPublication"],
                        },
                        lang: {
                            type: "string",
                        },
                    },
                    required: ["xsi:type"],
                    additionalProperties: false,
                },
                publicationTime: {
                    $ref: "#/definitions/DateTime",
                },
                publicationCreator: {
                    $ref: "#/definitions/InternationalIdentifier",
                },
                headerInformation: {
                    $ref: "#/definitions/HeaderInformation",
                },
                elaboratedData: {
                    items: {
                        $ref: "#/definitions/ElaboratedData",
                    },
                    type: "array",
                },
            },
            required: ["$", "publicationTime", "publicationCreator", "elaboratedData"],
            additionalProperties: false,
        },

        DateTime: {
            type: "string",
            format: "date-time",
        },

        HeaderInformation: {
            type: "object",
            properties: {
                confidentiality: {
                    type: "string",
                },
                informationStatus: {
                    type: "string",
                },
            },
        },

        ElaboratedData: {
            type: "object",
            properties: {
                source: {
                    type: "object",
                    properties: {
                        sourceIdentification: {
                            type: "string",
                        },
                    },
                },
                basicData: {
                    $ref: "#/definitions/BasicData",
                },
            },
        },

        BasicData: {
            type: "object",
            properties: {
                $: {
                    type: "object",
                    properties: {
                        "xsi:type": {
                            type: "string",
                            enum: ["TrafficStatus", "TrafficSpeed", "TravelTimeData"],
                        },
                    },
                    required: ["xsi:type"],
                    additionalProperties: false,
                },
                measurementOrCalculationTime: {
                    type: "string",
                },
                pertinentLocation: {
                    $ref: "#/definitions/PertinentLocation",
                },
                trafficStatusExtension: {
                    $ref: "#/definitions/TrafficStatusExtension",
                },
                travelTime: {
                    $ref: "#/definitions/TravelTime",
                },
                freeFlowTravelTime: {
                    $ref: "#/definitions/FreeFlowTravelTime",
                },
                freeFlowSpeed: {
                    $ref: "#/definitions/FreeFlowSpeed",
                },
                averageVehicleSpeed: {
                    $ref: "#/definitions/AverageVehicleSpeed",
                },
            },
            required: ["$", "measurementOrCalculationTime", "pertinentLocation"],
            additionalProperties: false,
        },

        PertinentLocation: {
            type: "object",
            properties: {
                $: {
                    type: "object",
                    properties: {
                        "xsi:type": {
                            type: "string",
                            enum: ["LocationByReference"],
                        },
                    },
                    required: ["xsi:type"],
                },
                predefinedLocationReference: {
                    type: "object",
                    properties: {
                        $: {
                            type: "object",
                            properties: {
                                targetClass: {
                                    type: "string",
                                },
                                id: {
                                    type: "string",
                                },
                                version: {
                                    type: "string",
                                },
                            },
                            required: ["targetClass", "id", "version"],
                        },
                    },
                    required: ["$"],
                },
            },
            required: ["$", "predefinedLocationReference"],
        },

        TrafficStatusExtension: {
            type: "object",
            properties: {
                ndicFcdExtension: {
                    type: "object",
                    properties: {
                        trafficLevel: {
                            type: "object",
                            properties: {
                                $: {
                                    type: "object",
                                    properties: {
                                        supplierCalculatedDataQuality: {
                                            type: "string",
                                        },
                                        numberOfInputValuesUsed: {
                                            type: "string",
                                        },
                                    },
                                    required: ["supplierCalculatedDataQuality", "numberOfInputValuesUsed"],
                                },
                                trafficLevelValue: {
                                    type: "string",
                                },
                            },
                            required: ["$", "trafficLevelValue"],
                        },
                    },
                    required: ["trafficLevel"],
                },
                queueInformation: {
                    type: "object",
                    properties: {
                        queueLength: {
                            type: "string",
                        },
                        fromPoint: {
                            type: "string",
                        },
                        toPoint: {
                            type: "string",
                        },
                    },
                    required: ["queueLength", "fromPoint", "toPoint"],
                },
            },
            required: ["ndicFcdExtension"],
            additionalProperties: false,
        },

        TravelTime: {
            type: "object",
            properties: {
                $: {
                    type: "object",
                    properties: {
                        supplierCalculatedDataQuality: {
                            type: "string",
                        },
                        numberOfInputValuesUsed: {
                            type: "string",
                        },
                    },
                    required: ["supplierCalculatedDataQuality", "numberOfInputValuesUsed"],
                    additionalProperties: false,
                },
                duration: {
                    type: "string",
                },
            },
            required: ["$", "duration"],
            additionalProperties: false,
        },

        FreeFlowTravelTime: {
            type: "object",
            properties: {
                $: {
                    type: "object",
                    properties: {
                        supplierCalculatedDataQuality: {
                            type: "string",
                        },
                        numberOfInputValuesUsed: {
                            type: "string",
                        },
                    },
                    required: ["supplierCalculatedDataQuality", "numberOfInputValuesUsed"],
                    additionalProperties: false,
                },
                duration: {
                    type: "string",
                },
            },
            required: ["duration"],
            additionalProperties: false,
        },

        FreeFlowSpeed: {
            type: "object",
            properties: {
                $: {
                    type: "object",
                    properties: {
                        supplierCalculatedDataQuality: {
                            type: "string",
                        },
                        numberOfInputValuesUsed: {
                            type: "string",
                        },
                    },
                    required: ["supplierCalculatedDataQuality", "numberOfInputValuesUsed"],
                    additionalProperties: false,
                },
                speed: {
                    type: "string",
                },
            },
            required: ["speed"],
            additionalProperties: false,
        },

        AverageVehicleSpeed: {
            type: "object",
            properties: {
                $: {
                    type: "object",
                    properties: {
                        supplierCalculatedDataQuality: {
                            type: "string",
                        },
                        numberOfInputValuesUsed: {
                            type: "string",
                        },
                    },
                    required: ["supplierCalculatedDataQuality", "numberOfInputValuesUsed"],
                    additionalProperties: false,
                },
                speed: {
                    type: "string",
                },
            },
            required: ["$", "speed"],
            additionalProperties: false,
        },
    },
};
