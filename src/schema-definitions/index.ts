import { datasourceFloatingCarDataSchema } from "#sch/schemas/fcd_datasource_schema";
import {
    outputFloatingCarDataSchema,
    outputFloatingCarDataSDMA,
    outputFloatingCarDataSDMAWithOsmPath,
} from "#sch/schemas/fcd_output_schema";

const forExport: any = {
    name: "FCD",
    pgSchema: "fcd",
    fcd_info: {
        name: "FloatingCarData",
        datasourceSchema: datasourceFloatingCarDataSchema,
        outputSchema: outputFloatingCarDataSchema,
        outputSequelizeAttributes: outputFloatingCarDataSDMA,
        outputSequelizeAttributesWithOsmPath: outputFloatingCarDataSDMAWithOsmPath,
        pgTableName: "fcd_traff_params_part",
    },
};

export { forExport as FCD };
