export interface IFloatingCarDataModel {
    publication_time: string;
    source_identification: string;
    measurement_or_calculation_time: string;
    predefined_location: string;
    version_of_predefined_location: number;
    traffic_level: string;

    // ⬐ Not required
    queue_exists?: boolean;
    queue_length?: number;
    from_point?: number;
    to_point?: number;

    data_quality: number;
    input_values: number;
    average_vehicle_speed: number;
    travel_time: number;
    free_flow_travel_time: number;
    free_flow_speed: number;
}

export interface IFloatingCarWithOsmPathDataModel extends IFloatingCarDataModel {
    osm_path?: string;
}

export interface IOutputApiFloatingCarDataElaboratedData {
    measurementOrCalculationTime: string;
    sourceIdentification: string;
    supplierCalculatedDataQuality: string;
    numberOfInputValuesUsed: string;
    predefinedLocationId: string;
    trafficLevel: string;
    averageVehicleSpeed: string;
    travelTime: string;
    freeFlowTravelTime: string;
    freeFlowSpeed: string;
    queueExists?: string;
    queueLength?: string;
    fromPoint?: string;
    toPoint?: string;
    osmPath?: number[] | null;
}
