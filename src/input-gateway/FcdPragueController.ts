import { FCD } from "#sch";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { AbstractFcdController } from "./AbstractFcdController";

export class FcdPragueController extends AbstractFcdController {
    protected saveQueueKey = "saveFloatingCarData";

    constructor() {
        super(new JSONSchemaValidator("FcdPragueInputValidation", FCD.fcd_info.datasourceSchema));
    }
}
