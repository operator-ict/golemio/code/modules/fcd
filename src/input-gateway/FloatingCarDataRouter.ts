import { FcdPragueController } from "#ig/FcdPragueController";
import { checkContentTypeMiddleware } from "@golemio/core/dist/input-gateway/helpers/CheckContentTypeMiddleware";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { AbstractFcdController } from "./AbstractFcdController";
import { FcdRegionsController } from "./FcdRegionsController";

export class FloatingCarDataRouter {
    public router: Router = Router();

    private fcdPragueController = new FcdPragueController();
    private fcdRegionsController = new FcdRegionsController();

    constructor() {
        this.initRoutes();
    }

    private initRoutes = (): void => {
        this.router.post("/fcd-info", checkContentTypeMiddleware(["text/xml", "application/xml"]), this.fcdPrague);
        this.router.post("/fcd-info-regions", checkContentTypeMiddleware(["text/xml", "application/xml"]), this.fcdRegions);
    };

    private fcdPrague = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        await this.floatingCarData(this.fcdPragueController, req, res, next);
    };

    private fcdRegions = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        await this.floatingCarData(this.fcdRegionsController, req, res, next);
    };

    private floatingCarData = async (
        controller: AbstractFcdController,
        req: Request,
        res: Response,
        next: NextFunction
    ): Promise<void> => {
        try {
            await controller.processData(req.body);

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}

const floatingCarDataRouter = new FloatingCarDataRouter().router;

export { floatingCarDataRouter };
