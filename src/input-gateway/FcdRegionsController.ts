import { FCD } from "#sch";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { AbstractFcdController } from "./AbstractFcdController";

export class FcdRegionsController extends AbstractFcdController {
    protected saveQueueKey = "saveRegionsData";

    constructor() {
        super(new JSONSchemaValidator("FcdRegionsInputValidation", FCD.fcd_info.datasourceSchema));
    }
}
