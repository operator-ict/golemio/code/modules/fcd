## **G O L E M I O - OTEVŘENÝ NÁVOD NA PRAKTICKÉ POUŽITÍ LOKALIZAČNÍCH TABULEK**

## Obsah

1. [Záměr Golemio otevřené uživatelské dokumentace](#záměr-golemio-otevřené-uživatelské-dokumentace)
1. [Vstupní data](#vstupní-data)
1. [Parametry vrstvy `POINTS`](#parametry-vrstvy-points)
1. [Oprávnění](#oprávnění)
1. [Opendata](#opendata)
1. [Vizualizace dat](#vizualizace-dat)
1. [Ostatní](#ostatní)

## Záměr Golemio otevřené uživatelské dokumentace
Záměrem tohoto specifického popisu je především praktické sdílení technického pohledu na ty části a parametry jinak komplexního geolokalizačního souboru datové sady `Lokalizačních tabulek` (dále LT) tak, jak jsou skutečně prakticky využívány v projektech společnosti Operátor ICT, a.s. (dále OICT), potažmo v jeho oddělení datové platformy Golemio.

Hlavním cílem dokumentu je jednoduché a pochopitelné vysvětlení základních a reálně používaných parametrů datové sady tak, aby toto použití bylo více zřejmé a jednoduššíc pro nové kolegy, firemní dodavatele či partnery.

Jednoduše jde o vypíchnutí a uvedení těch konkrétních technických tipů a podělení se o doposud nabyté **know-how**, jak s potřebnými částmi datové sady LT efektivně pracovat.

## Vstupní data
Původní obecný popis integrace datové sady v Golemiu a celkový náhled do ní samotné je detailně popsán v intením dokumentu **Obecný popis datové sady Lokalizačních tabulek** a není součástí tohoto otevřeného návodu na praktické použití Lokalizačních tabulek (více info viz níže kapitola „Ostatní“).

### Datová sada obsahuje čtyři GIS vrstvy:

Jedná se o statické vektorové mapové podklady, které jsou spravovány a aktualizovány vlastníkem Ředitelstvím silnic a dálnic s. p. (dále ŘSD).

<details>
<summary><b>ADMINS<b></summary>
NEPOUŽÍVANÉ vůbec (v OICT) - členění administrativních oblastí na území České republiky. Vrstva nese databázi geometrie hranic a další atributy těchto oblastí.
</details>

<details>
<summary><b>POINTS<b></summary>
POUŽÍVANÉ především (v OICT) - hraniční body příslušící k „ROADS“ nebo k „SEGMENTS“, které definují počáteční a koncový bod jednoho TMC úseku (neboli objektu v „ROADS“ nebo v „SEGMENTS“). Body jsou spojeny vždy přímkami, LT nenesou infrormaci o reálném geometrickém průběhu pozemní komunikace (silnice, ulice apod.).
</details>

<details>
<summary><b>ROADS<b></summary>
NEPOUŽÍVANÉ prakticky (v OICT) - přímky vedené mezi svými hraničními body ve vrstvě „POINTS“. Využití je zde pouze vizuální, uživatelské nad mapou pro lepší přehled situace.
</details>

<details>
<summary><b>SEGMENTS<b></summary>
NEPOUŽÍVANÉ vůbec (v OICT) - vrstva definující více spojených přímek „ROADS“ v jednom unikátním objektu.
</details>

**Z uvedených vrstev využívá OICT prakticky pouze jednu vrstvu - `POINTS`**. Pro pouze vizuální potřeby a orientační zobrazení uživateli nad mapou využívá OICT ještě vrstvu `ROADS`, nikoli však pro výpočetní zpracování a další strojové úkony.

### Ukázka vybraných vstupních dat
Ukázka jednoho z původních datových souborů ve formátů *.txt pro popisovanou vrstvu `POINTS` (formát *.txt byl již dříve zvolen pro integraci na Golemiu):

<details>
<summary>POINTS header</summary>
CID;TABCD;LCD;CLASS;TCD;STCD;JNUMBER;ROADNUMBER;ROADNAME;FIRSTNAME;SECONDNAME;AREA_REF;AREA_NAME;ROA_LCD;SEG_LCD;ROA_TYPE;INPOS;OUTPOS;INNEG;OUTNEG;PRESENTPOS;PRESENTNEG;INTERRUPT;URBAN;INT_LCD;NEG_OFF;POS_OFF;WGS84_X;WGS84_Y;SJTSK_X;SJTSK_Y;ISOLATED
</details>

<details>
<summary>POINTS data</summary>
11;25;13978;P;6;12;;;;Plavecký stadion Strakonice;;30671;Strakonice;;;;0;0;0;0;0;0;;1;;;;13,91387;49,26284;-791878,8597;-1128708,97;1
</details>

**Grafická ukázka pouze vrstvy bodů** (takto samostatně působí na uživatele nejasně)
![**Grafická ukázka pouze vrstvy bodů** (takto samostatně působí na uživatele nejasně)](./assets/LT/LT_body.jpg)

**Společné grafické zobrazení vrstvy `POINTS` i vrstvy `ROADS`** (vizuální spojení bodů liniemi už dává větší smysl)
![**Společné grafické zobrazení vrstvy `POINTS` i vrstvy `ROADS`** (vizuální spojení bodů liniemi už dává větší smysl)](./assets/LT/LT_body+useky.jpg)

Statiská vrstva `POINTS` obsahuje aktuálně všechna **potřebná data pro získání geolokalizačních informací k přesnému umístění časoprostorových dynamických dopravních dat, událostí, informací apod., která jsou v OICT získávána od dodavatele ŘSD**. Současně jde o tzv. Floating Car Data (dále FCD) a data z Národního dopravního informačího centra (dále NDIC).


## Parametry vrstvy `POINTS`

V podstatě **jde ve většině případech o silniční křížení** nebo pak o geometrická zalomení v plochém prostoru podle průběhů dlouhých ulic a silnic.

<details>
<summary>Původní zobrazení náhodně vybraného bodu s parametry v prostředí GIS v ulici Argentinská</summary>
![Konkrétní vybraný bod s parametry](./assets/LT/LT_QGIS_vybrany_bod.jpg)
</details>

### Parametry a jejich význam

Nadřazený parametr
- `CID`: tzv. Country Identifier; jednoznačný kód používaný pro identifikaci země nebo regionu, tedy zeměpisné oblasti, ke které se lokalizační tabulka vztahuje. Parametr prakticky nenabývá jiných hodnot, než 11, což je identifikátor České republiky.

Vnořené parametry každého `CID`
- `TABCD`: Table Code; představuje číselný identifikátor přiřazený konkrétní sadě lokalizačních tabulek RDS-TMC (Radio Data System - Traffic Message Channel) v rámci země. ČR má přidělený rozsah `TABCD` 25 až 28.
- `LCD`: Location Code; ID; jedinečný číselný identifikátor pro každé umístění hraničního bodu (definuje počátek či konec „svého“ úseku).
- `CLASS`: klasifikace podle kategorie objektu; A = administrativní oblasti typu země, regionu apod.; L = lineární prvky představující silnice; P = reprezentace formou bodů, např. křižovatky.
- `TCD`: Type Code; klasifikační systém podtypů objektů z `CLASS`, podle umístění bodů; např. P1 = značí všechny typy křižovatek, P3 = pokrývá různé orientační body atd.
- `STCD`: Subtype Code; dále více dělí objekty na podkategorie z `TCD`, např. P1.1 definuje konkrétně dálniční křižovatku, P1.8 kruhový objezd, P1.10 křižovatku se světelnou signalizací atd.
- `JNUMBER`: Junction Number; identifikuje křižovatky, především dálniční sjezdy, pomocí čísla odvozeného z kilometrového značení křižovatky; dálniční staničení.
- `ROADNUMBER`: číslo silnice; značení podle konvencí dané země nebo širšího regionu (v ČR např. zančení D1, I/42 apod.).
- `ROADNAME`: název silnice; zvláště relevantní pro místní komunikace bez vyplněného `ROADNUMBER`.
- `FIRSTNAME`: primární název bodu; váže se obvykle k názvu úzeku ulice či silnice, kterou ohraničuje (např. Průmyslová).
- `SECONDNAME`: sekundární název; často poskytuje místní kontext nebo doplňující informace.
- `AREA_REF`: definuje specifické unikátní číslo dané administativní oblasti, ve které se bod nachází; např. 203.
- `AREA_NAME`: definuje slovní název administrativní oblasti odpovídající `AREA_REF` spojené s bodem; např. Praha-Dolní Počernice.
- `ROA_LCD`: kód nadřazené pozemní komunikace, ke které bod patří; příslušnost k úseku linie podle `LCD`.
- `SEG_LCD`: Segment Location Code; příslušnost bodu k segmentu pozemní komunikace, ke kterému patří.
- `ROA_TYPE`: bod přísluší k pozemní komunikaci podle zavedené třídy, např. označení „D“ pro dálnice, „I/“ pro silnice první třídy atd.; název může být např. i složený L1.1 (dálnice).
- `INPOS`, `OUTPOS`, `INNEG`, `OUTNEG`: příznaky označující povolené směry vjezdu a výjezdu vzhledem k bodu a dopravnímu proudu.
- `PRESENTPOS`, `PRESENTNEG`: příznaky potvrzující existenci bodu v kladném a záporném směru; obvykle parametr nabývá hodnoty 1, ale např. v případě osamoceného bodu dálničního nájezdu nebo sjezdu nabývá 0.
- `INTERRUPT`: kód lokace bodu, kde začíná přerušení silnice; zdánlivě souvislý úsek/silnice je přerušen/a na více dílčích úseků.
- `URBAN`: příznak označující, zda bod představuje primárně městské (1) nebo meziměstské (0) dopravní prostředí; intravilán vs extravilán.
- `INT_LCD`: výčet kódů všech `LCD`, které se v zákrytu nacházejí ve stejné lokalitě bodu jedné křižovatky.
- `NEG_OFF_LCD`, `POS_OFF_LCD`: kódy lokací předcházejícího a následujícího bodu podél linie.
- `WGS84_X`, `WGS84_Y`: souřadnice umístění bodu ve formátu WGS 84; též GPSka.
- `SJTSK_X`, `SJTSK_Y`: souřadnice umístění bodu ve formátu JTSK.
- `ISOLATED`: označení, zda je bodové umístění považováno za „izolované“; že není přímo spojeno s konkrétním segmentem silnice.


**Více `CID` bodů je obvykle ve vrstvě `POINTS` kladeno v zákrytu přes sebe, což není na první pohled v mapovém podkladu vidět a tento fakt může být pro neznalého uživatele velmi matoucí až nelogický při práci s datovou sadou.** Každý takový bod má však stále své unikátní `LCD` číslo.

![**Body v zákrytu**](./assets/LT/LT_body_v_zakrytu.jpg)

Dosavadní informace by měly běžnému uživateli stačit k velmi rychlému pochopení logiky struktury a práce s Lokalizačními tabulkami v OICT a na datové platformě Golemio. Dále následují doplňující informace, které mohou ještě pomoci k doplnění představené dokumentace.

## Oprávnění

Otevřená vlastní dokumentace OICT (Golemia).

## Opendata

Tento rychlý návod byl vytvořen v OICT na datové platformě Golemio na základě vlastních zkušeností z práce s Lokalizačními tabulkami a z nastudovaného know-how původní dokumentace Lokalizačních tabulek od společnosti CEDA Maps, a.s. Je tak volně šiřitelný. Dokument není v této chvíli umístěn v žádném open data katalogu.

Datová sada Lokalizačních tabulek volně šiřitelná není. Smlouva s dodavatelem CEDA Maps a.s. to OICT neumožnuje.

## Vizualizace dat

Pro vizualizaci datové sady používá aktuálně OICT (Golemio) softwarový nástroj QGIS.

## Ostatní
Další doplňující informace k datové sadě včetně jejího původního popisu integrace v Golemiu jsou dále více rozvedeny v intením dokumentu **Obecný popis datové sady Lokalizačních tabulek**, který není součástí tohoto otevřeného návodu. Pro přístup i k tomuto dokumentu můžete případně zažádat formou e-mailu na adresách `info@operatorict.cz` nebo `derbek.premysl@operatorict.cz`.
