# Implementační dokumentace modulu *fcd*

## Záměr

modul slouzi k ukladaci a retenci FCD dat


## Vstupní data

### Data nám jsou posílána

Mame vystavene 2 endpointy viz [openApi spec](./openapi-input.yaml)

## Zpracování dat / transformace

Vsechny tabulky jsou ve schematu `fcd`

Interní RabbitMQ fronty jsou popsány v [AsyncAPI](./asyncapi.yaml).

### *FCDWorker*

Fcd modul ma jen jednoho workera ktery se stara o ukladani dat a retenci dat

#### _task: RegionsDataRetentionTask_

Vola proceduru `regions_data_retention` s parametrem 2880 -> tzn smaze data stare vice jak 2 dny

- vstupní rabbitmq fronta
  - nazev: dataplatform.fcd.regionsDataRetention
  - parametry: zadne
  - TTL: 59
- (volitelně) závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
  - zadne
- datové zdroje
  - zadne
- transformace
  - zadne
- (volitelně) obohacení dat
  - zadne
- data modely
  - zadne


#### _task: SaveFloatingCarDataTask_

uklada data do tabulky `fcd_traff_params_part`

- vstupní rabbitmq fronta
  - nazev: dataplatform.fcd.saveFloatingCarData
  - [parametry](./assets/schemas/floating_car_data_schema.xml)
- (volitelně) závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
  - zadne
- datové zdroje
  - zadne
- transformace
  - [FloatingCarDataTransformation](../src/integration-engine//transformations/FloatingCarDataTransformation.ts)
- (volitelně) obohacení dat
  - zadne
- data modely
  - [FloatingCarDataRepository](../src/integration-engine/repositories/fcd/FloatingCarDataRepository.ts)


#### _task: SaveRegionsDataTask_

uklada data do tabulky `fcd_traff_params_regions`
- vstupní rabbitmq fronta
  - název: dataplatform.fcd.saveRegionsData
  - [parametry](./assets/schemas/floating_car_data_schema.xml)
- (volitelně) závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
  - zadne
- datové zdroje
  - zadne
- transformace
  - [FloatingCarDataTransformation](../src/integration-engine//transformations/FloatingCarDataTransformation.ts)
- (volitelně) obohacení dat
  - zadne
- data modely
  - [FloatingCarDataRegionsRepository](../src/integration-engine/repositories/fcd/FloatingCarDataRegionsRepository.ts)


## Uložení dat

-   typ databáze
  -   PSQL
-   databázové schéma
    -   [fcd schema](./assets/db_schema.png)
        -   `fcd_traff_params_part`: Obsahuje fcd data z Hlavního města Praha a Středočeského kraje. Historická data se zachovávají.
        -   `fcd_traff_params_regions`: Obsahuje fcd data z krajů: Liberecký, Královehradecký, Pardubický, Vysočina, Jihočeský, Plzeňský, Karlovarský, Ústecký. Integrační engine pouští pravidelnou retenci dat(`regions_data_retention`).

## Output API

### Obecné

- OpenAPI dokumentace
  - [openapi-output](./openapi-output.yaml)
- veřejné / neveřejné endpointy
  - api je veřejné nebo neveřejné, případně seznam veřejných a neveřejných endpointů


#### _/v2/fcd/info_

- zdrojové tabulky
  - je pouzit view `v_traffic_params_all` kde jste spojene tabulky `fcd_traff_params_regions` a `fcd_traff_params_part`
  - pri zadosti o osmPath je pouzit view `v_traffic_params_all_with_osm_path`/`v_traffic_params_last_hour_osm_path` kde je pripojena tabulka `traffic.rsd_tmc_osm_mapping` pro ziskani `osm_path`
- zobrazi data viz [openapi](./openapi-output.yaml)
