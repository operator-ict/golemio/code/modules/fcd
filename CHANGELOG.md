# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.2.12] - 2024-10-16

### Added

-   AsyncAPI documentation ([integration-engine#262](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/262))

## [1.2.11] - 2024-09-12

### Changed

-   `.gitlab-ci.yml` cleanup ([devops#320](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/320))

## [1.2.10] - 2024-09-09

### Removed

-   metrics due to poor performance([fcd#15](https://gitlab.com/operator-ict/golemio/code/modules/fcd/-/issues/15))

## [1.2.9] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.2.8] - 2024-06-03

### Added

-   add cache-control header to all responses ([core#106](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/106))

### Removed

-   remove redis useCacheMiddleware ([core#106](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/106))

## [1.2.7] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.2.6] - 2024-04-29

### Changed

-   Optimization of fcd/info endpoint ([fcd#14](https://gitlab.com/operator-ict/golemio/code/modules/fcd/-/issues/12))

## [1.2.5] - 2024-04-15

### Changed

-   Rework worker to use AbstractWorker class ([core#14](https://gitlab.com/operator-ict/golemio/code/modules/fcd/-/issues/14))

## [1.2.4] - 2024-03-25

### Fixed

-   Fixed router query validation rules ([core#93](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/93))

## [1.2.3] - 2024-01-15

### Removed

-   Unused tables fcd_geos_90, tmc_ltcze90_roads_wgs84

## [1.2.2] - 2023-09-27

### Changed

-   API versioning - version moved to path ([core#80](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/80))

## [1.2.1] - 2023-08-30

-   No changelog

## [1.2.0] - 2023-08-23

### Changed

-   Run integration apidocs tests via Golemio CLI ([core#46](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/46))

## [1.1.10] - 2023-07-31

### Changed

-   Replaced moment.js with dateTime helper ([core#68](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/68))

## [1.1.9] - 2023-07-24

### Added

-   Adding portman and docs for the input gateway.

## [1.1.6] - 2023-06-21

### Added

-   new `minutesBefore` query filter for /fcd/info route [p0131#134](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/134)

## [1.1.5] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.1.4] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))
-   IE processing of regions data and OG output together with current data. ([ipt#128](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/128))

## [1.1.3] - 2023-05-29

### Added

-   input gateway router and controller for regions data ([ipt#128](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/128))

### Changed

-   Use RsdTmcOsmMappingRepository from traffic-common module ([ipt#123](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/123))

## [1.1.2] - 2023-05-03

### Changed

-   Adjust IG logger emitter imports

## [1.1.1] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.10] - 2022-11-03

### Added

-   Add TMC net

### Changed

-   Update TypeScript to v4.7.2

## [1.0.9] - 2022-09-21

### Removed

-   Unused dependencies ([general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [1.0.8] - 2022-07-27

### Fixed

-   new lookup table for Praha Dopravni project added fcd_geos_90

## [1.0.7] - 2022-05-25

### Fixed

-   fix/rsd-osm-mapping

## [1.0.6] - 2022-05-02

### Changed

-   Separate PSQL schema

## [1.0.5] - 2022-03-31

### Fixed

-   Output API - return only latest `measurementOrCalculationTime` data ([fcdProject#39](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/39))

## [1.0.4] - 2022-03-09

### Fixed

-   Input Data Validation Schema ([fcd#5](https://gitlab.com/operator-ict/golemio/code/modules/fcd/-/issues/5))
-   Accidental Mongo connection in api docs tests

## [1.0.3] - 2022-02-16

### Changed

-   Table name to public.fcd_traf_params_part ([fcd#4](https://gitlab.com/operator-ict/golemio/code/modules/fcd/-/issues/4))

## [1.0.2] - 2022-01-05

### Fixed

-   Output format of Osm_path
-   Transformation of queue info

## [1.0.1] - 2021-12-01

### Added

-   Swagger doc
-   Portman tests

## [1.0.0] - 2021-11-30

### Added

-   Create the FCD module

